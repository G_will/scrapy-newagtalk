# -*- coding: utf-8 -*-

import csv
import pymysql


def export_csv_topic():
    DB = {
        'host': 'localhost',
        'port': 33060,
        'user': 'homestead',
        'passwd': 'secret',
        'db': 'newagtalk'
    }

    db_conn = pymysql.connect(**DB)
    db_cur = db_conn.cursor()

    select_sql = 'select tid,fid,title,posted_at,user_name,user_location from topic;'
    db_cur.execute(select_sql)
    select_res = db_cur.fetchall()

    with open('topic.csv', 'wb') as f:
        writer = csv.writer(f)
        writer.writerow(['tid', 'fid', 'title', 'posted_at', 'user_name', 'user_location'])
        writer.writerows(select_res)

    db_cur.close()
    db_conn.close()


def export_csv_message():
    DB = {
        'host': 'localhost',
        'port': 33060,
        'user': 'homestead',
        'passwd': 'secret',
        'db': 'newagtalk'
    }

    db_conn = pymysql.connect(**DB)
    db_cur = db_conn.cursor()

    select_sql = 'select mid,tid,fid,posted_at,user_name,user_location,content from message;'
    db_cur.execute(select_sql)
    select_res = db_cur.fetchall()

    with open('message.csv', 'wb') as f:
        writer = csv.writer(f)
        writer.writerow(['mid', 'tid', 'fid', 'posted_at', 'user_name', 'user_location', 'content'])
        writer.writerows(select_res)

    db_cur.close()
    db_conn.close()


def export_csv_20150510():
    DB = {
        'host': 'localhost',
        'user': 'root',
        'passwd': '',
        'db': 'newagtalk'
    }

    db_conn = pymysql.connect(**DB)
    db_cur = db_conn.cursor()

    print("Query Message")
    select_sql = "select mid,tid,posted_at,user_name,user_location,content from message where posted_at >= '2015-01-01' order by mid;"
    db_cur.execute(select_sql)
    select_res = db_cur.fetchall()

    with open('message-20150510.csv', 'wb') as f:
        writer = csv.writer(f)
        writer.writerow(['mid', 'tid', 'posted_at', 'user_name', 'user_location', 'content'])
        writer.writerows(select_res)
    print('Export Message')

    print("Query Topic")
    select_sql = "select tid,title,posted_at,user_name,user_location from topic where posted_at >= '2015-01-01' order by tid;"
    db_cur.execute(select_sql)
    select_res = db_cur.fetchall()

    with open('topic-20150510.csv', 'wb') as f:
        writer = csv.writer(f)
        writer.writerow(['mid', 'tid', 'posted_at', 'user_name', 'user_location', 'content'])
        writer.writerows(select_res)
    print('Export Topic')

    db_cur.close()
    db_conn.close()


if __name__ == '__main__':
    export_csv_topic()
    export_csv_message()