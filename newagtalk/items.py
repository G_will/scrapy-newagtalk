# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy import Item
from scrapy import Field


class NewagtalkItem(Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass


class TopicItem(Item):
    tid = Field()
    fid = Field()
    title = Field()
    posted_at = Field()
    user_name = Field()
    user_location = Field()


class MessageItem(Item):
    mid = Field()
    tid = Field()
    fid = Field()
    posted_at = Field()
    user_name = Field()
    user_location = Field()
    content = Field()