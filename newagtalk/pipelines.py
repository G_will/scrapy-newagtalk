# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from pony.orm import db_session

from scrapy.utils.project import get_project_settings
from scrapy import log

from newagtalk.models import db
from newagtalk.models import TopicEntity, MessageEntity
from newagtalk.items import TopicItem, MessageItem

settings = get_project_settings()


def info(msg):
    log.msg(msg, level=log.WARNING)


#class NewagtalkPipeline(object):
#    def process_item(self, item, spider):
#        return item


class CropTalkPipeline(object):

    def __init__(self):
        db.bind('mysql', **settings.get('DB'))
        db.generate_mapping()

    def process_item(self, item, spider):

        if isinstance(item, TopicItem):
            with db_session:
                topicEntity = TopicEntity.get(tid = item['tid'])

                if topicEntity:
                    info('already have this topic: %s' % item['tid'])
                    return item

                topicEntity = TopicEntity(
                    tid = item['tid'],
                    fid = item['fid'],
                    title = item['title'],
                    posted_at = item['posted_at'],
                    user_name = item['user_name'],
                    user_location = item['user_location']
                )

                info('save topic: %s' % item['tid'])

        if isinstance(item, MessageItem):
            with db_session:
                messageEntity = MessageEntity.get(mid = item['mid'])

                if messageEntity:
                    info('already have this message: %s' % item['mid'])
                    return item

                messageEntity = MessageEntity(
                    mid = item['mid'],
                    tid = item['tid'],
                    fid = item['fid'],
                    posted_at = item['posted_at'],
                    user_name = item['user_name'],
                    user_location = item['user_location'],
                    content = item['content']
                )

                info('save message: %s' % item['mid'])

        return item

    def spider_closed(self, spider):
        db.disconnect()