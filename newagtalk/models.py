# -*- coding: utf-8 -*-

from datetime import datetime
from pony.orm import *

db = Database()


class TopicEntity(db.Entity):

    _table_ = 'topic'

    tid = PrimaryKey(int)
    fid = Required(int)
    title = Required(unicode)
    posted_at = Required(datetime)
    user_name = Required(unicode)
    user_location = Optional(unicode)


class MessageEntity(db.Entity):

    _table_ = 'message'

    mid = PrimaryKey(int)
    tid = Required(int)
    fid = Required(int)
    posted_at = Required(datetime)
    user_name = Required(unicode)
    user_location = Optional(unicode)
    content = Required(LongUnicode)

