# -*- coding: utf-8 -*-

# Scrapy settings for newagtalk project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'newagtalk'

SPIDER_MODULES = ['newagtalk.spiders']
NEWSPIDER_MODULE = 'newagtalk.spiders'

DB = {
    'host': 'localhost',
    'port': 33060,
    'user': 'homestead',
    'passwd': 'secret',
    'db': 'newagtalk'
}

DOWNLOADER_MIDDLEWARES = {
    'newagtalk.middlewares.CustomUserAgentMiddleware': 401,
}

ITEM_PIPELINES = {
    'newagtalk.pipelines.CropTalkPipeline': 300,
}

DOWNLOAD_DELAY = 0.8

LOG_LEVEL = 'WARNING'

#COOKIES_ENABLED = False

#CLOSESPIDER_ITEMCOUNT = 22000